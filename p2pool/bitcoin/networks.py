import os
import platform

from twisted.internet import defer

from . import data
from p2pool.util import math, pack, jsonrpc

@defer.inlineCallbacks
def check_genesis_block(bitcoind, genesis_block_hash):
    try:
        yield bitcoind.rpc_getblock(genesis_block_hash)
    except jsonrpc.Error_for_code(-5):
        defer.returnValue(False)
    else:
        defer.returnValue(True)

nets = dict(

    whirlcoin=math.Object(
        P2P_PREFIX='f9beb4d4'.decode('hex'),
        P2P_PORT=48481,
        ADDRESS_VERSION=36,
        RPC_PORT=48480,
        RPC_CHECK=defer.inlineCallbacks(lambda bitcoind: defer.returnValue(
            'WhirlCoinaddress' in (yield bitcoind.rpc_help()) and
            not (yield bitcoind.rpc_getinfo())['testnet']
        )),
        SUBSIDY_FUNC=lambda nBits, height: __import__('whirlcoin_subsidy').getBlockBaseValue(nBits, height+1),
        POW_FUNC=lambda data: pack.IntType(256).unpack(__import__('whirlcoin_hash').getHash(data, 80)),
        BLOCK_PERIOD=45, # s
        SYMBOL='WHRL',
        CONF_FILE_FUNC=lambda: os.path.join(os.path.join(os.environ['APPDATA'], 'WhirlCoin') if platform.system() == 'Windows' else os.path.expanduser('~/Library/Application Support/WhirlCoin/') if platform.system() == 'Darwin' else os.path.expanduser('~/.whirlcoin'), 'whirlcoin.conf'),
        BLOCK_EXPLORER_URL_PREFIX='',
        ADDRESS_EXPLORER_URL_PREFIX='',
        TX_EXPLORER_URL_PREFIX='',
        SANE_TARGET_RANGE=(2**256//2**32//1000 - 1, 2**256//2**20 - 1),
        DUMB_SCRYPT_DIFF=1,
        DUST_THRESHOLD=0.001e8,
    ),

)
for net_name, net in nets.iteritems():
    net.NAME = net_name
