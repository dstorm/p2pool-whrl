from p2pool.bitcoin import networks
from p2pool.util import math

# CHAIN_LENGTH = number of shares back client keeps
# REAL_CHAIN_LENGTH = maximum number of shares back client uses to compute payout
# REAL_CHAIN_LENGTH must always be <= CHAIN_LENGTH
# REAL_CHAIN_LENGTH must be changed in sync with all other clients
# changes can be done by changing one, then the other

nets = dict(

    whirlcoin=math.Object(
        PARENT=networks.nets['whirlcoin'],
        SHARE_PERIOD=15, # seconds
        CHAIN_LENGTH=24*60*60//15, # shares
        REAL_CHAIN_LENGTH=24*60*60//15, # shares
        TARGET_LOOKBEHIND=80, # shares  //with that the pools share diff is adjusting faster, important if huge hashing power comes to the pool
        SPREAD=40, # blocks
        IDENTIFIER='e93105a2c97c4139'.decode('hex'),
        PREFIX='d285c095245164d5'.decode('hex'),
        P2P_PORT=47481,
        MIN_TARGET=0,
        MAX_TARGET=2**256//2**20 - 1,
        PERSIST=False,
        WORKER_PORT=47480,
        BOOTSTRAP_ADDRS='whrl.altmine.net'.split(' '),
        ANNOUNCE_CHANNEL='#p2pool-whrl',
        VERSION_CHECK=lambda v: True,
    ),

)
for net_name, net in nets.iteritems():
    net.NAME = net_name
