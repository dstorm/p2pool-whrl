from distutils.core import setup, Extension

whirlcoin_module = Extension('whirlcoin_subsidy', sources = ['whirlcoin_subsidy.cpp'])

setup (name = 'whirlcoin_subsidy',
       version = '1.0',
       description = 'Subsidy function for WhirlCoin',
       ext_modules = [whirlcoin_module])
