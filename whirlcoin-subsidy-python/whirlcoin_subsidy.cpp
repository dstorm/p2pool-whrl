#include <Python.h>

static const int64_t COIN = 100000000;

int64_t static GetBlockBaseValue(int nBits, int nHeight)
{
    float block;

    block = static_cast<double>(nHeight);

    double reward = -sqrt(block * 0.0029) + 100.0;

    double nSubsidy = reward;

    if (block == 10){
        nSubsidy = 3400000;
    }

    nSubsidy *= COIN;

    return nSubsidy;
}

static PyObject *whirlcoin_subsidy_getblockbasevalue(PyObject *self, PyObject *args)
{
    int input_bits;
    int input_height;
    if (!PyArg_ParseTuple(args, "ii", &input_bits, &input_height))
        return NULL;
    long long output = GetBlockBaseValue(input_bits, input_height);
    return Py_BuildValue("L", output);
}

static PyMethodDef whirlcoin_subsidy_methods[] = {
    { "getBlockBaseValue", whirlcoin_subsidy_getblockbasevalue, METH_VARARGS, "Returns the block value" },
    { NULL, NULL, 0, NULL }
};

PyMODINIT_FUNC initwhirlcoin_subsidy(void) {
    (void) Py_InitModule("whirlcoin_subsidy", whirlcoin_subsidy_methods);
}
